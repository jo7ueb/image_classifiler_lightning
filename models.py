from vgg import *
from alexnet import *

models = {
    "alexnet":  alexnet,
    "vgg11":    vgg11,
    "vgg11_bn": vgg11_bn,
    "vgg13":    vgg13,
    "vgg13_bn": vgg13_bn,
    "vgg16":    vgg16,
    "vgg16_bn": vgg16_bn,
    "vgg19":    vgg19,
    "vgg19_bn": vgg19_bn,
          }

def setup_model(name, num_classes=1000):
    if name in models:
        fn = models[name]
    else:
        raise RuntimeError(f"Unknown model name {name}")

    return fn(num_classes=num_classes)
