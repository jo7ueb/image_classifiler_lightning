import torch
import torch.nn as nn

__all__ = [
    "vgg11",
    "vgg11_bn",
    "vgg13",
    "vgg13_bn",
    "vgg16",
    "vgg16_bn",
    "vgg19",
    "vgg19_bn",
    ]

class VGG(nn.Module):
    def __init__(
            self, features, num_classes = 1000, init_weights = True, dropout = 0.5
            ):
        super().__init__()
        self.features = features
        self.classifier = nn.Sequential(
            nn.Linear(512 * 7 * 7, 4096),
            nn.ReLU(True),
            nn.Dropout(p=dropout),
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Dropout(p=dropout),
            nn.Linear(4096, num_classes),
        )

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    nn.init.kaiming_normal_(m.weight, mode="fan_in", nonlinearity="relu")
                    if m.bias is not None:
                        nn.init.constant_(m.bias, 0)
                elif isinstance(m, nn.BatchNorm2d):
                    nn.init.constant_(m.weight, 1)
                    nn.init.constant_(m.bias, 0)
                elif isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x

def make_layers(cfg, batch_norm = False):
    layers = []
    in_channels = 3
    for v in cfg:
        if v == "M":
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            v = int(v)
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)

cfgs = {
    "A": [64, "M", 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "B": [64, 64, "M", 128, 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "D": [64, 64, "M", 128, 128, "M", 256, 256, 256, "M", 512, 512, 512, "M", 512, 512, 512, "M"],
    "E": [64, 64, "M", 128, 128, "M", 256, 256, 256, 256, "M", 512, 512, 512, 512, "M", 512, 512, 512, 512, "M"],
}

def _vgg(cfg, batch_norm, weights, progress, **kwargs):
    model = VGG(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model

def vgg11(weights=None, progress=True, **kwargs):
    return _vgg("A", False, weights, progress, **kwargs)

def vgg11_bn(weights=None, progress=True, **kwargs):
    return _vgg("A", True, weights, progress, **kwargs)

def vgg13(weights=None, progress=True, **kwargs):
    return _vgg("B", False, weights, progress, **kwargs)

def vgg13_bn(weights=None, progress=True, **kwargs):
    return _vgg("B", True, weights, progress, **kwargs)

def vgg16(weights=None, progress=True, **kwargs):
    return _vgg("D", False, weights, progress, **kwargs)

def vgg16_bn(weights=None, progress=True, **kwargs):
    return _vgg("D", True, weights, progress, **kwargs)

def vgg19(weights=None, progress=True, **kwargs):
    return _vgg("E", False, weights, progress, **kwargs)

def vgg19_bn(weights=None, progress=True, **kwargs):
    return _vgg("E", True, weights, progress, **kwargs)
