import torch
import lightning as L

from train import LModelWrapper
from models import setup_model

class LModelWrapper(L.LightningModule):
    def __init__(self, model):
        super().__init__()
        self.model = model

    def forward(self, x):
        return self.model(x)

MODEL_PATH='./model.pt'
MODEL = 'alexnet'

model = setup_model(MODEL)
model = LModelWrapper(model)

torch.save(model.model.state_dict(), MODEL_PATH)
