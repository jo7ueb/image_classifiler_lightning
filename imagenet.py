from typing import Any

import torch
from torch.utils.data import DataLoader
import torchvision
import torchvision.transforms.v2 as T
from torchvision.datasets import ImageFolder
import lightning as L

class Imagenet1kDataset(L.LightningDataModule):
    def __init__(self, config: Any):
        super().__init__()
        self.train_transform = T.Compose(
            [
                T.RandomResizedCrop(size=(224, 224), antialias=True),
                T.RandomHorizontalFlip(),
                T.ToImage(),
                T.ToDtype(torch.float32, scale=True),
                T.Normalize(mean=[0.4811, 0.4575, 0.4078], std=[0.2234, 0.2294, 0.2302]),
            ]
        )

        self.eval_transform = T.Compose(
            [
                T.Resize((224, 224)),
                T.ToImage(),
                T.ToDtype(torch.float32, scale=True),
                T.Normalize(mean=[0.4811, 0.4575, 0.4078], std=[0.2234, 0.2294, 0.2302]),
            ]
        )

        self.dataset_train = ImageFolder(
            root=config.data_path,
            transform=self.train_transform,
        )
        self.dataset_eval = ImageFolder(
            root=config.data_path_eval,
            transform=self.eval_transform,
        )

        def collate_fn(batch):
            imgs = torch.stack([x[0] for x in batch])
            labels = torch.tensor([x[1] for x in batch])
            return imgs, labels

        self.dataloader_train = DataLoader(
            self.dataset_train,
            batch_size=config.batch_size,
            shuffle=True,
            num_workers=config.num_workers,
            prefetch_factor=2,
            pin_memory=True
        )
        self.dataloader_eval = DataLoader(
            self.dataset_eval,
            batch_size=config.eval_batch_size,
            shuffle=False,
            num_workers=config.num_workers,
            prefetch_factor=2,
            pin_memory=True
        )

    def train_dataloader(self):
        return self.dataloader_train

    def val_dataloader(self):
        return self.dataloader_eval
